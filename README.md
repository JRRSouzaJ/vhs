# VHS #
## _Uma ferramenta leve, simples e intuitiva_ ##

Esta aplicação é uma ferramente de lado cliente que possibilita a captura de tela com ou sem áudio para projetos simples de gravação.

- Leve
- Gratuito
- Fácil

Veja o projeto funcionando em: 

- Gitlab: https://jrrsouzaj.gitlab.io/vhs/

## Recursos ##

- Permite capturar toda a tela, uma janela ou uma guia
- Permite gravar a partir de uma webcam
- Permite capturar áudio via microfone
- Exporta a gravação

## Tecnologias ##

Este projeto usa as segintes tecnologias:

- [HTML5] - Para estrutura do site
- [CSS] - Para estilo visual
- [System.css] - Biblioteca de UI retrô
- [JavaScript] - Para controle do fluxo de dados
- [JQuery] - Para manipulação do DOM

## License ##

2022 - Futura Criativa

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <https://www.gnu.org/licenses/>.


[//]: # (Referências utilizadas)

   [jQuery]: <http://jquery.com>
   [JavaScript]: <https://www.javascript.com>
   [HTML5]: <https://html.spec.whatwg.org/>
   [CSS]: <https://www.w3.org/Style/CSS>
   [System.css]: <https://github.com/sakofchit/system.css>
